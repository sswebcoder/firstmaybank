<?php

/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 14.12.14
 * Time: 1:23
 */

defined('ROOT') or die('No direct script access.');

class ControllerOrder
{

    public function index()
    {

        $order = new ModelOrder();
        $saved = false;
        $validated = false;

        if (isset($_POST['order'])) {

            $order->attributes = $_POST['order'];
            $validated = $order->validate();
            if ($validated)
                $saved = $order->save();
        }

        $this->render('orderForm', array(
            'validated' => $validated,
            'saved' => $saved,
            'data' => $order->attributes,
            'errors' => $order->getValidateErrors()
        ));

    }

    public function render($tmplName, $data)
    {
        ViewOrder::render($tmplName, $data);
    }

}