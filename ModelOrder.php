<?php

/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 14.12.14
 * Time: 1:29
 */

defined('ROOT') or die('No direct script access.');

class ModelOrder
{

    public $attributes;
    public $last_name;
    public $first_name;
    public $saucer_number;
    public $date;
    public $client_number;
    public $comment;
    protected $_validated = false;
    protected $_validateErrors = array();

    public function validate()
    {
        foreach ($this->attributes as $key => $value) {
            $this->{$key} = trim($value);
        }

        $this->last_name = ucfirst($this->last_name);
        $this->first_name = ucfirst($this->first_name);
        $this->saucer_number = str_replace(' ', '', $this->saucer_number);
        $this->saucer_number = mb_strtoupper($this->saucer_number);

        !$this->checkEmpty('last_name', 'Введите фамилию. Анонимно летающие тарелки не принимаются.')
        && $this->checkByPattern('last_name', '/^([A-z-\s]+)$/', 'Фамилия должна начинаться с большой буквы, допустимы только латинские символы разделённые пробелом или дефисом. ');

        !$this->checkEmpty('first_name', 'Введите имя. Анонимно летающие тарелки не принимаются.')
        && $this->checkByPattern('first_name', '/^([A-z-\s]+)$/', 'Имя должно начинаться с большой буквы, допустимы только латинские символы разделённые пробелом или дефисом. ');

        !$this->checkEmpty('saucer_number', 'Введите регистрационный номер летающей тарелки')
        && $this->checkByPattern('saucer_number', '/^([A-z]{1}[\d]{5}[A-z]{2})$/', 'Номер летающей тарелки должен иметь формат a12356bc.');

        !$this->checkEmpty('date', 'Необходимо указать дату ремонта')
        && $this->checkDate('date', 'Введите дату в формате ДД.ММ.ГГГГ');

        !$this->checkEmpty('client_phone_number', 'Укажите номер для связи')
        && $this->checkByPattern('client_phone_number', '/^[A-Fa-f0-9]{12}$/', 'Телекоммуникационный номер должен состоять из 12 шестнадцатиричных чисел');

        return $this->hasValidateError();
    }

    protected function checkEmpty($fieldName, $message)
    {
        if ($this->{$fieldName}) {
            return false;
        } else {
            $this->addValidateError($fieldName, $message);
            return true;
        }
    }

    protected function checkByPattern($fieldName, $pattern, $message)
    {
        if (!preg_match($pattern, $this->{$fieldName}))
            $this->addValidateError($fieldName, $message);

    }

    protected function checkDate($fieldName, $message)
    {
        list($day, $month, $year) = preg_split('/\./', $this->{$fieldName});
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            $this->addValidateError($fieldName, $message);
        }
    }

    public function save()
    {
        $xml = new SimpleXMLElement('<xml/>');
        $order = $xml->addChild('order');
        foreach ($this->attributes as $key => $value) {
            $order->addChild($key, $this->{$key});
        }

        $filename = $this->date . '-' . uniqid() . '.xml';
        return $xml->asXML('xml/' . $filename);
    }

    protected function addValidateError($field, $message)
    {
        $this->_validateErrors[$field] = $message;
    }

    protected function hasValidateError()
    {
        return !(bool)count($this->_validateErrors);
    }

    public function getValidateErrors()
    {
        return $this->_validateErrors;
    }

}