<?php defined('ROOT') or die('No direct script access.'); ?>

<!DOCTYPE html>
<html lang="ru">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<title>Заявка на ремонт - сервис по ремонту летающих тарелок</title>

<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="/css/datepicker.css">

<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/locales/bootstrap-datepicker.ru.js"></script>
<head>

</head>
<body>


<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
    <?php if($validated && $saved): ?>
        <br>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Заказ оформлен</h3>
            </div>
            <div class="panel-body">
                <p>Спасибо, что воспользовались нашим сервисом. Мы свяжемся с вами по указанному вами межгалактическому
                телекоммуникационному номеру для уточнения деталей.</p>
            </div>
        </div>

        <?php elseif($validated): ?>
        <br>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Ошибка сохранения</h3>
            </div>
            <div class="panel-body">
                <p>Произошла ошибка при сохранении xml данных в файл. Скорее всего данная
                ошибка вызвана отсутствием прав на запись у HTTP сервера в директорию xml.</p>
            </div>
        </div>
        <?php else: ?>


        <h1>Форма заказа</h1>
        <?php if($validated) echo 'Данные валидны'; ?>
        <?php if($saved) echo 'Данные сохранены'; ?>

        <form role="form" method="post">
            <div class="form-group <?php if (isset($errors['last_name'])): ?> has-error<?php endif; ?>">
                <label class="control-label" for="last_name">Фамилия марсианина<sup class="text-danger">*</sup></label>
                <input name="order[last_name]" type="text" class="form-control" id="last_name" placeholder="Last name"
                       value="<?php echo $data['last_name'] ?>">
                <?php if (isset($errors['last_name'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errors['last_name'] ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group <?php if (isset($errors['last_name'])): ?> has-error<?php endif; ?>">
                <label class="control-label" for="first_name">Имя марсианина<sup class="text-danger">*</sup></label>
                <input name="order[first_name]" type="text" class="form-control" id="first_name"
                       placeholder="First name"
                       value="<?php echo $data['first_name'] ?>">
                <?php if (isset($errors['first_name'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errors['first_name'] ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group <?php if (isset($errors['saucer_number'])): ?> has-error<?php endif; ?>">
                <label class="control-label" for="saucer_number">Номер летающей тарелки<sup class="text-danger">*</sup></label>
                <input name="order[saucer_number]" type="text" class="form-control" id="saucer_number"
                       placeholder="Number of flying saucer"
                       value="<?php echo $data['saucer_number'] ?>">
                <?php if (isset($errors['saucer_number'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errors['saucer_number'] ?>
                    </div>
                <?php endif; ?>

            </div>

            <div class="form-group <?php if (isset($errors['date'])): ?> has-error<?php endif; ?>">
                <label class="control-label" for="date">Дата проведения ремонта<sup class="text-danger">*</sup></label>

                <div class="input-group date">
                    <input name="order[date]" type="text" class="form-control" id="date"
                           placeholder="Date(click for chose)"
                           value="<?php echo $data['date'] ?>">
                        <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
                <?php if (isset($errors['date'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errors['date'] ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group  <?php if (isset($errors['client_phone_number'])): ?> has-error<?php endif; ?>">
                <label class="control-label" for="client_phone_number">МТНЗ<sup class="text-danger">*</sup></label>
                <input name="order[client_phone_number]" type="text" class="form-control" id="client_phone_number"
                       placeholder="ITCN"
                       value="<?php echo $data['client_phone_number'] ?>">
                <?php if (isset($errors['client_phone_number'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errors['client_phone_number'] ?>
                    </div>
                <?php endif; ?>

            </div>

            <div class="form-group">
                <label for="comment">Комментарий к заказу</label>
                <textarea name="order[comment]" class="form-control" rows="3"
                          placeholder="Comment"><?php echo $data['comment'] ?></textarea>
            </div>

            <button type="submit" class="btn btn-default pull-right">Заказать</button>
            <br><br><br><br>
        </form>
<?php endif; ?>

    </div>
    <div class="col-md-3"></div>
</div>

<script>
    $(document).ready(function () {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            language: "ru",
            autoclose: true
        });
    });
</script>

</body>
</html>

