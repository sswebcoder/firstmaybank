<?php

/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 14.12.14
 * Time: 1:29
 */

defined('ROOT') or die('No direct script access.');

class ViewOrder
{

    public static function render($tmplName, $data)
    {
        if (isset($data) && count($data)) {
            foreach ($data as $key => $value) {
                ${$key} = $value;
            }
        }

        include $tmplName . '.php';
    }

}